import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Buku Digitalku 4.0'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: true,
        body: SingleChildScrollView(
//          width: double.infinity,
//          height: double.infinity,
          child: Column(
            children: <Widget>[
              headerSection,
              menuSection,
              materiSection,
              announcementSection,
              lastStudySection,
            ],
          ),
        ));
  }

  Widget headerSection = Container(
      width: double.infinity,
      height: 320.0,
      child: Stack(
        children: <Widget>[
          Image.asset(
            'assets/top_home.png',
            width: double.infinity,
            fit: BoxFit.cover,
          ),
          Positioned(
            top: 0,
            left: 0,
            child: Container(
              margin: const EdgeInsets.only(top: 20.0, left: 20.0),
              child: Icon(
                Icons.menu,
                color: Colors.white,
                size: 28.0,
              ),
            ),
          ),
          Positioned(
            top: 0,
            child: Container(
              margin:
                  const EdgeInsets.only(top: 20.0, left: 100.0, right: 100.0),
              child: Text('Buku Digitalku 4.0',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 25.0)),
            ),
          ),
          Positioned(
              top: 0,
              right: 0,
              child: Container(
                margin: EdgeInsets.only(top: 20.0, right: 20.0),
                child: Icon(
                  Icons.search,
                  color: Colors.white,
                  size: 28.0,
                ),
              )),
          Positioned(
            top: 100.0,
            left: 16.0,
            child: Container(
              child: Text('Hai !',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.normal,
                      fontSize: 18.0)),
            ),
          ),
          Positioned(
            top: 120.0,
            left: 16.0,
            child: Container(
              child: Text('Adam',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0)),
            ),
          ),
          Positioned(
            top: 160.0,
            left: 16.0,
            child: Container(
              child: ClipOval(
                child: Image.asset(
                  "assets/person.jpg",
                  height: 80,
                  width: 80,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Positioned(
            top: 220.0,
            left: 120.0,
            child: Container(
              width: 90.0,
              height: 90.0,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  // You can use like this way or like the below line
                  //borderRadius: new BorderRadius.circular(30.0),
                  color: Color.fromRGBO(54, 53, 99, 1)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Kelas',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.normal,
                        fontSize: 16.0),
                  ),
                  Text(
                    'XI',
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 30.0),
                  ),
                ],
              ),
            ),
          ),
        ],
      ));
  Widget menuSection = Container(
    child: Column(
      children: <Widget>[
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                child: Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(top: 10.0),
                  width: 250.0,
                  height: 50.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: new BorderRadius.only(
                          topRight: Radius.circular(30.0),
                          bottomRight: Radius.circular(30.0)),
                      color: Colors.red),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Matematika',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.normal,
                                fontSize: 20.0)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 20.0, top: 10.0, bottom: 10.0),
                        child: Icon(
                          Icons.photo,
                          size: 33.0,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 16.0),
                width: 100.0,
                height: 50.0,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      child: Container(
                        child: ClipOval(
                          child: Image.asset(
                            "assets/person2.jpg",
                            height: 50,
                            width: 50,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 40.0,
                      child: Container(
                        child: ClipOval(
                          child: Image.asset(
                            "assets/person3.jpg",
                            height: 50,
                            width: 50,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                child: Container(
                  alignment: Alignment.centerLeft,
                  width: 250.0,
                  height: 50.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: new BorderRadius.only(
                          topRight: Radius.circular(30.0),
                          bottomRight: Radius.circular(30.0)),
                      color: Colors.green),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Biologi',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.normal,
                                fontSize: 20.0)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 20.0, top: 10.0, bottom: 10.0),
                        child: Icon(
                          Icons.photo,
                          size: 33.0,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 16.0),
                width: 100.0,
                height: 50.0,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      child: Container(
                        child: ClipOval(
                          child: Image.asset(
                            "assets/person2.jpg",
                            height: 50,
                            width: 50,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 40.0,
                      child: Container(
                        child: ClipOval(
                          child: Image.asset(
                            "assets/person3.jpg",
                            height: 50,
                            width: 50,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 1.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                child: Container(
                  alignment: Alignment.centerLeft,
                  margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                  width: 250.0,
                  height: 50.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: new BorderRadius.only(
                          topRight: Radius.circular(30.0),
                          bottomRight: Radius.circular(30.0)),
                      color: Colors.blue),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Fisika',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.normal,
                                fontSize: 20.0)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 20.0, top: 10.0, bottom: 10.0),
                        child: Icon(
                          Icons.photo,
                          size: 33.0,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 16.0),
                width: 100.0,
                height: 50.0,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      child: Container(
                        child: ClipOval(
                          child: Image.asset(
                            "assets/person2.jpg",
                            height: 50,
                            width: 50,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 40.0,
                      child: Container(
                        child: ClipOval(
                          child: Image.asset(
                            "assets/person3.jpg",
                            height: 50,
                            width: 50,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 1.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                child: Container(
                  alignment: Alignment.centerLeft,
                  width: 250.0,
                  height: 50.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: new BorderRadius.only(
                          topRight: Radius.circular(30.0),
                          bottomRight: Radius.circular(30.0)),
                      color: Colors.yellow),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Kimia',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.normal,
                                fontSize: 20.0)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 20.0, top: 10.0, bottom: 10.0),
                        child: Icon(
                          Icons.photo,
                          size: 33.0,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 16.0),
                width: 100.0,
                height: 50.0,
                child: Stack(
                  children: <Widget>[
                    Positioned(
                      child: Container(
                        child: ClipOval(
                          child: Image.asset(
                            "assets/person2.jpg",
                            height: 50,
                            width: 50,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Positioned(
                      left: 40.0,
                      child: Container(
                        child: ClipOval(
                          child: Image.asset(
                            "assets/person3.jpg",
                            height: 50,
                            width: 50,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(top: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                alignment: Alignment.topLeft,
                child: Container(
                  alignment: Alignment.centerLeft,
                  width: 250.0,
                  height: 50.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      borderRadius: new BorderRadius.only(
                          topRight: Radius.circular(30.0),
                          bottomRight: Radius.circular(30.0)),
                      color: Colors.grey),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Lainnya',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.normal,
                                fontSize: 20.0)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 20.0, top: 10.0, bottom: 10.0),
                        child: Icon(
                          Icons.photo,
                          size: 33.0,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    ),
  );
  Widget materiSection = Container(
    child: Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(16.0),
              child: Text('Materi Terbaru',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 22.0)),
            ),
            Container(
              margin: EdgeInsets.all(16.0),
              child: Text('Lihat Semua',
                  style: TextStyle(
                      color: Colors.deepPurple,
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0)),
            ),
          ],
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Container(
            height: 120,
            child: Row(
              children: <Widget>[
                Container(
                  width: 200,
                  height: 120,
                  margin: EdgeInsets.only(left: 16.0, bottom: 16.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: YoutubePlayer(
                      controller: YoutubePlayerController(
                        initialVideoId: '_h96r-cKCvI',
                        flags: YoutubePlayerFlags(
                          autoPlay: false,
                          mute: true,
                        ),
                      ),
                      showVideoProgressIndicator: true),
                ),
                Container(
                  width: 200,
                  height: 120,
                  margin: EdgeInsets.only(left: 16.0, bottom: 16.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: YoutubePlayer(
                      controller: YoutubePlayerController(
                        initialVideoId: 'uBChMjDpXB4',
                        flags: YoutubePlayerFlags(
                          autoPlay: false,
                          mute: true,
                        ),
                      ),
                      showVideoProgressIndicator: true),
                ),
                Container(
                  width: 200,
                  height: 120,
                  margin: EdgeInsets.only(left: 16.0, bottom: 16.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                  ),
                  child: YoutubePlayer(
                      controller: YoutubePlayerController(
                        initialVideoId: '2Ie0k4l3m_o',
                        flags: YoutubePlayerFlags(
                          autoPlay: false,
                          mute: true,
                        ),
                      ),
                      showVideoProgressIndicator: true),
                ),
              ],
            ),
          ),
        )
      ],
    ),
  );
  Widget announcementSection = Container(
    child: Column(
      children: <Widget>[
        Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 16.0),
              child: Text('Pengumuman Tugas',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 22.0)),
            ),
            Container(
              margin: EdgeInsets.only(left: 5.0),
              child: Text('(99+)',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.normal,
                      fontSize: 15.0)),
            ),
            Container(
              margin: EdgeInsets.only(
                  left: 25.0, bottom: 16.0, top: 16.0, right: 16.0),
              child: Text('Lihat Semua',
                  style: TextStyle(
                      color: Colors.deepPurple,
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0)),
            ),
          ],
        ),
        Container(
          width: double.infinity,
          height: 145,
          margin: EdgeInsets.only(top: 8.0, left: 16.0, right: 16.0),
          child: Card(
            elevation: 5,
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 16.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0),
            ),
            child: Container(
              height: 145,
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Tugas Seni Budaya',
                          style: TextStyle(
                              color: Colors.black54,
                              fontWeight: FontWeight.bold),
                        ),
                        Icon(
                          Icons.close,
                          size: 15,
                          color: Colors.black,
                        )
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
                    child: Text(
                      'Dikumpulkan di folder google drive, link di grup wa',
                      style: TextStyle(
                          color: Colors.black54, fontWeight: FontWeight.normal),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
                      child: Divider(
                        color: Colors.black54,
                      )),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
                    child: Text(
                      'Batas Pengumpulan tugas - 12.00 WIB',
                      style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.normal,
                          fontSize: 12),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
                    child: Text(
                      'Arini S.Pd',
                      style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.normal,
                          fontSize: 12),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          width: double.infinity,
          height: 145,
          margin: EdgeInsets.only(top: 8.0, left: 16.0, right: 16.0),
          child: Card(
            elevation: 5,
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 16.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0),
            ),
            child: Container(
              height: 145,
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          'Tugas Seni Budaya',
                          style: TextStyle(
                              color: Colors.black54,
                              fontWeight: FontWeight.bold),
                        ),
                        Icon(
                          Icons.close,
                          size: 15,
                          color: Colors.black,
                        )
                      ],
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
                    child: Text(
                      'Dikumpulkan di folder google drive, link di grup wa',
                      style: TextStyle(
                          color: Colors.black54, fontWeight: FontWeight.normal),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
                      child: Divider(
                        color: Colors.black54,
                      )),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
                    child: Text(
                      'Batas Pengumpulan tugas - 12.00 WIB',
                      style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.normal,
                          fontSize: 12),
                    ),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
                    child: Text(
                      'Arini S.Pd',
                      style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.normal,
                          fontSize: 12),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    ),
  );
  Widget lastStudySection = Container(
    child: Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 16.0),
              child: Text('Belajar Terakhir',
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 22.0)),
            ),
            Container(
              margin: EdgeInsets.all(16.0),
              child: Text('Lihat Semua',
                  style: TextStyle(
                      color: Colors.deepPurple,
                      fontWeight: FontWeight.bold,
                      fontSize: 15.0)),
            ),
          ],
        ),
        Container(
          width: double.infinity,
          height: 100,
          margin: EdgeInsets.only(top: 8.0, left: 16.0, right: 16.0),
          child: Card(
            elevation: 5,
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 16.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0),
            ),
            child: Container(
              height: 100,
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          height: 60,
                          width: 80,
                          decoration: BoxDecoration(color: Colors.red),
                          child: YoutubePlayer(
                              controller: YoutubePlayerController(
                                initialVideoId: '_h96r-cKCvI',
                                flags: YoutubePlayerFlags(
                                  autoPlay: false,
                                  mute: true,
                                ),
                              ),
                              showVideoProgressIndicator: true),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
                              child: Text(
                                'Matematika',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.normal,
                                fontSize: 12),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
                              child: Text(
                                'Bab 1 Algoritma Dasar',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Colors.black, fontWeight: FontWeight.bold),
                              ),
                            ),

                            Container(
                              margin: EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
                              child: Text(
                                'Terakhir di buka pada, Senin 8 Juni 2020',
                                style: TextStyle(
                                    color: Colors.black54,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12),
                              ),
                            ),
                          ],
                        ),

                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          width: double.infinity,
          height: 100,
          margin: EdgeInsets.only(top: 8.0, left: 16.0, right: 16.0),
          child: Card(
            elevation: 5,
            margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 16.0),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0.0),
            ),
            child: Container(
              height: 100,
              child: Row(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 8.0, left: 8.0, right: 8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          height: 60,
                          width: 80,
                          decoration: BoxDecoration(color: Colors.red),
                          child: YoutubePlayer(
                              controller: YoutubePlayerController(
                                initialVideoId: 'uBChMjDpXB4',
                                flags: YoutubePlayerFlags(
                                  autoPlay: false,
                                  mute: true,
                                ),
                              ),
                              showVideoProgressIndicator: true),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
                              child: Text(
                                'Matematika',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.normal,
                                fontSize: 12),
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
                              child: Text(
                                'Bab 1 Algoritma Dasar',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Colors.black, fontWeight: FontWeight.bold),
                              ),
                            ),

                            Container(
                              margin: EdgeInsets.only(top: 5.0, left: 8.0, right: 8.0),
                              child: Text(
                                'Terakhir di buka pada, Senin 8 Juni 2020',
                                style: TextStyle(
                                    color: Colors.black54,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 12),
                              ),
                            ),
                          ],
                        ),

                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    ),
  );
}
